import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {triviaCategoriesModel} from "../models/triviaCategories.model";
import {triviaDataModel} from "../models/triviaData.model";
import {HttpClient} from "@angular/common/http";
import {categoriesModel} from "../models/categories.model";
import {triviaModel} from "../models/trivia.model";
import {map} from "rxjs";
import {Router} from "@angular/router";
import {ResultsService} from "../services/results.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  myForm: FormGroup = new FormGroup({});
  categories: triviaCategoriesModel[] = [];
  triviaData: triviaDataModel[] = [];
  difficulty: FormControl = new FormControl('');
  postedAnswers = ['', '', '', '', ''];
  showSubmitBtn = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private rService: ResultsService
  ) {}

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    const categoryUrl = 'https://opentdb.com/api_category.php';
    this.http.get<categoriesModel>(categoryUrl)
      .subscribe(response => {
        this.categories = response.trivia_categories;
      })
  }

  shuffle(array: string[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  };

  create(categoryId: string, difficulty: string) {
    const triviaUrl = 'https://opentdb.com/api.php?amount=5&category=' + categoryId + '&difficulty=' + difficulty + '&type=multiple';
    this.http.get<triviaModel>(triviaUrl)
      .pipe(
        map(val => {
          val.results.map(data => {
            data.answers = []; // add array element answers
            data.answers.push(data.correct_answer); // add correct answer to answers
            data.incorrect_answers.forEach((element: string) => data.answers.push(element)); // add incorrect_answers to answers

            return this.shuffle(data.answers); // return randomized answers
          })
          return val;
        })
      )
      .subscribe(response => {
        this.triviaData = response.results;
      })
  }

  onButtonClick(event: Event, triviaId: number) {
    let selectedAnswer = (event.target as HTMLInputElement).innerText;
    this.postedAnswers[triviaId] = selectedAnswer;

    // if postedAnswers has all the answers => show submit button
    if (this.postedAnswers.every(a => a !== '')) {
      this.showSubmitBtn = true;
    }
  }

  submit() {
    this.rService.setPostedAnswers(this.postedAnswers);
    this.rService.setTriviaData(this.triviaData);

    this.router.navigate(['results'])
  }
}
