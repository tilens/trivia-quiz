import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ResultsService} from "../services/results.service";
import {triviaDataModel} from "../models/triviaData.model";
import {resultsDataModel} from "../models/resultsData.model";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnDestroy {
  answers: string[] = [];
  triviaData: triviaDataModel[] = [];
  resultsData: resultsDataModel = {
    correct_answers: 0,
    data: [],
  };
  rServiceAnsSub = new Subscription();
  rServiceTriSub = new Subscription();

  constructor(
    private rService: ResultsService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.rServiceAnsSub = this.rService.getPostedAnswers().subscribe(answers => {
      this.answers = answers;
    });
    this.rServiceTriSub = this.rService.getTriviaData().subscribe(triviaData => {
      this.triviaData = triviaData;
    });

    this.resultsData = this.rService.createOutputArray(this.triviaData, this.answers);
  }

  getBackgroundColor() {
    if (this.resultsData.correct_answers > 3) {
      return 'green';
    } else if (this.resultsData.correct_answers > 1) {
      return 'yellow';
    } else {
      return 'red';
    }
  }

  newQuiz() {
    this.router.navigate(['index'])
  }

  ngOnDestroy() {
    this.rServiceAnsSub.unsubscribe();
    this.rServiceTriSub.unsubscribe();
  }
}
