import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatChipsModule} from "@angular/material/chips";
import {MatButtonModule} from "@angular/material/button";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResultsComponent } from './results/results.component';
import {AppRoutingModule} from "./app-routing.module";
import { IndexComponent } from './index/index.component';
import {MatCardModule} from "@angular/material/card";
import { DecodeHtmlStringPipe } from './decode-html-string.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ResultsComponent,
    IndexComponent,
    DecodeHtmlStringPipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
