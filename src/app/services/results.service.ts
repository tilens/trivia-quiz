import {Injectable, OnDestroy} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {triviaDataModel} from "../models/triviaData.model";
import {resultsDataModel} from "../models/resultsData.model";

@Injectable({
  providedIn: 'root', // can use anywhere
})
export class ResultsService {
  postedAnswers = new BehaviorSubject<string[]>([]);
  triviaData = new BehaviorSubject<triviaDataModel[]>([]);
  resultsData: resultsDataModel = {
    correct_answers: 0,
    data: [],
  };

  constructor() {}

  setPostedAnswers(postedAnswers: string[]) {
    this.postedAnswers.next(postedAnswers);
  }

  getPostedAnswers(): Observable<string[]> {
    return this.postedAnswers.asObservable();
  }

  setTriviaData(triviaData: triviaDataModel[]) {
    this.triviaData.next(triviaData);
  }

  getTriviaData(): Observable<triviaDataModel[]> {
    return this.triviaData.asObservable();
  }

  createOutputArray(triviaData: triviaDataModel[], answers: string[]) {
    this.resultsData.correct_answers = 0;

    triviaData.map((trivia, i) => {
      this.resultsData.data[i] = {
        answers: [],
        question: '',
      };

      trivia.answers.map((answer, j) => {
        this.resultsData.data[i].answers[j] = {
          type: '',
          text: ''
        }

        this.resultsData.data[i].answers[j].text = answer;
        if (answer == answers[i] && answer == trivia.correct_answer) {
          console.log('Bingo')
          console.log(answer+ ' == ' + answers[i]+ ' && '+answer+ ' == ' +trivia.correct_answer)
          this.resultsData.data[i].answers[j].type = 'bingo';
          this.resultsData.correct_answers++;
        } else if (answer == trivia.correct_answer){
          this.resultsData.data[i].answers[j].type = 'correct';
        } else if (answer == answers[i]){
          this.resultsData.data[i].answers[j].type = 'wrong';
        } else {
          this.resultsData.data[i].answers[j].type = 'default';
        }
      })
      this.resultsData.data[i].question = trivia.question;
    })

    return this.resultsData;
  }
}
