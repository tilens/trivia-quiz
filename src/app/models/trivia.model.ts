import {triviaDataModel} from "./triviaData.model";

export interface triviaModel {
  results: triviaDataModel[]
}
