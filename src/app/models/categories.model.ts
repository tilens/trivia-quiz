import {triviaCategoriesModel} from "./triviaCategories.model";

export interface categoriesModel {
  trivia_categories: triviaCategoriesModel[]
}
