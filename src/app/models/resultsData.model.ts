import {dataModel} from "./data.model";

export interface resultsDataModel {
  correct_answers: number;
  data: dataModel[];
}
