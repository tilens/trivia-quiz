import {answersModel} from "./answers.model";

export interface dataModel {
  question: string;
  answers: answersModel[]
}
